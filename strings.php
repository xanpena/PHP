<?php 

error_reporting(E_ALL);

$a = 'Hola';
$b = 'Manuel';
$c = $a . ' ' . $b; 
echo $c; //Resultado "Hola Manuel"
echo substr($c, 0 ,4); //Primeros 4 caracteres, resultado "Hola"
echo substr($c, 0, -2); //Resta últimos 2 caracteres, resultado "Hola Manu"
echo substr($c, 2, 4);  //Resultado "la M"
echo strlen($c); //Resultado 11
echo str_replace('Hola', 'Adios', $c); //Resultado "Adios Manuel"

echo '<br';

echo __FILE__; // Devuelve la ruta absoluta al directorio del fichero actual

echo dirname(__FILE__); // Devuelve la ruta absoluta al directorio del fichero actual

require(basename(__FILE__).'/miarchivophp'); // Incluye el archivo miarchivo.php que se encuentre en el mismo directorio que el fichero actual

$m2 = array('Azul', 'Amarillo', 'Rojo');
echo "<pre>";
	var_dump($m2);
echo "</pre>";

//echo "<pre>"; var_dump($m2); die;

$m3 = array(
	'color1'=>'naranja',
	'color2'=>'azul'
);

echo "El primer color es ".$proyecto['color1'];

$tablero = array(
	'fila1'=>array('O', 'O', '_'),
	'fila2'=>array('X', 'O', 'X'),
	'fila3'=>array('O', '_', 'X'),
);

echo '<br />Posición fila 3, columna 2:'.$tablero['fila3'][1];

$tablero2 = array(
	'fila1'=>array('O', 'O', '_'),
	'fila2'=>array('X', 'O', 'X'),
	'fila3'=>array('columna1'=>'O', 'columna2'=>'_', 'columna3'=>'X'),
);

echo '<br />Posición fila 3, columna 2:'.$tablero['fila3']['columna2'];

