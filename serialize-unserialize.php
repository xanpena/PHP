<?php

    class A {
        var $uno = 1;

        function mostrar_uno(){
            echo $this->uno;
        }
    }

    // pagina1.php

    include("clase_a.inc");

    $a = new A;
    $s = serialize($a);

    // almacenar $s en alguna parte en memoria donde pagina2.php lo pueda encontrar
    $fp = fopen("almacenamiento", "w");
    fwrite($fp, $s);
    fclose($fp);

    // pagina2.php

    include("clase_a.inc");

    $s = implode("", @file("almacenamiento"));
    $a = unserialize($s);

    // ahora podemos usar la función mostrar_uno() del objeto $a
    $a->mostrar_uno();

?>