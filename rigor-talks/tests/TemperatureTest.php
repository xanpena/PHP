<?php 

declare(strict_types=1);

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\Temperature; 

class TemperatureTest extends TestCase
{
    public function testTryToCreateANonValidTemperature()
    {
        $this->expectException(\Src\TemperatureNegativeException::class);
        Temperature::take(-1);
    }

    public function testTryToCreateAValidTemperature()
    {
        $measure = 19; 
        $this->assertSame(
            $measure,
            (Temperature::take($measure))->measure()
        );
    }
}