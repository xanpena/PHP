<?php

declare(strict_types=1);

namespace Src;

class TemperatureNegativeException extends \Exception 
{
    public static function fromMeasure(int $measure)
    {
        return new static(
            sprintf('Measure %d must be positive', $measure)
        );
    }
}

final class Temperature
{
    private int $measure;

    private function __construct(int $measure)
    {
        $this->setMeasure($measure);
    }

    private function setMeasure(int $measure)
    {
        $this->checkMeasureIsPositive($measure);
        $this->measure = $measure;
    }

    private function checkMeasureIsPositive(int $measure)
    {
        if ($measure < 0) {
            throw TemperatureNegativeException::fromMeasure($measure);
        }
    }

    public static function take(int $measure) : self
    {
        return new self($measure);
    }

    public function measure(): int
    {
        return $this->measure; 
    } 
    
}
