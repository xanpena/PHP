<?php // Realizado como parte del curso de PHP de OpenWebinars.net
error_reporting(E_ALL);
// una interfaz solo puede definir declaraciones de funciones
/// qué métodos por lo menos deben tener las clases que coincidan con  esta interfaz para ser válidas.
//Una clase no puede tener más de un padre pero si hereder más de una interfaz
interface ToBrowser{
	function __toString();
}

interface WithValue{
	function setValue($value);
}

// No se puede instanciar una clase abstracta.
// abstract: Clase de paso, intermedia
abstract class Printable implements ToBrowser{
	abstract function __toString();
}

class HTMLTag extends Printable implements ToBrowser{
	public $tagName = '';
	private $attributes = array();
	public function __toString(){
		$result = '<'.$this->tagName;
		foreach($this->attributes as $key => $value){
			$result .= ' '.$key.'="'.$value.'"';
		}
		$result .= '>';
		return $result;
	}

	public function __construct($tagName=''){
		if(!empty($tagName)) $this->tagName = $tagName;
	}

	public function setAttribute($name, $value){
		$this->attributes[$name]=$value;
	}
}

// Paso por el constructor $b = new HTMLTag('hr');
//$b = new HTMLTag();
//$b->tagName='hr';
//echo $b->__toString();

class HTMLInput extends HTMLtag implements ToBrowser, WithValue{
	public $tagName = 'input';
	protected $type;
	protected $labelText;

	public function setValue($newValue){
		$this->setAttribute('value', $newValue);
	}

	public function __construct($labelText=''){
		$this->labelText = $labelText;
		parent::__construct('');
		$this->setAttribute('type', $this->type);
	}

	public function __toString(){
		$result = $this->labelText.' ';
		$result .= parent::__toString();
		$result .= '<br>';
		return $result;
	}
}

/*
class HTMLInputCheckbox extends HTMLInput {
	public function __construct($labelText=''){
		parent::__construct($labelText);
		$this->setValue('type', 'checkbox');
	}
}
*/

class HTMLInputButton extends HTMLInput implements ToBrowser, WithValue {
	protected $type='button';
}

class HTMLInputCheckbox extends HTMLInput implements ToBrowser, WithValue {
	protected $type='checkbox';
}

class HTMLInputText extends HTMLInput implements ToBrowser, WithValue {
	protected $type='text';
}

class HTMLInputFactory{
	const TYPE_TEXT = 'text';
	static public function create($type, $labelText=''){
		switch($type){
			case 'button':
				$o = new HTMLInputButton();
				//$o->setValue($labelText);
			break;
			case 'text':
				$o = new HTMLInputText($labelText);
			break;
			case 'checkbox':
				$o = new HTMLInputCheckbox($labelText);
			break;
			default:
				return 'Tipo no reconocido';
		}
		return $o;
	}
}



$h1 = new HTMLTag('h1');
$h1_f = new HTMLTag('/h1');
echo $h1.'Ejemplo de formulario'.$h1_f;
//$i = new HTMLInput();
//$c = new HTMLInputCheckbox;
//$i->attributes['type']='button';
//$i->attributes['value']='Botón';
//$i->setInputValue('Mi botón');
//echo $h1.'Mola!'.$h1_f;
// echo $i;
// echo $c;

$i1 = HTMLInputFactory::create(HTMLInputFactory::TYPE_TEXT, 'Nombre');
$i2 = new HTMLInputText('Apellidos');
$i3 = new HTMLInputCheckbox('Acepta las condiciones');
$i4 = new HTMLInputButton();
$i4->setValue('Enviar');
$c = new HTMLInputCheckbox(); 


echo $i1.$i2.$i3.$i4;
