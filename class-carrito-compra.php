<?php

class Carrito {
    var $items;

    function agregar_item($artnr, $num){
        $this->items[$artnr] += $num;
    }

    function retirar_item($artnr, $num){
        if($this->items[$artnr] > $num){
            $this->items[$artnr] -= $num;
            return true;
        }else{
            return false;
        }

    }
}

?>