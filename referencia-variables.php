<?
// La variable $a toma el valor 4
$a = 4;
/* La variable $b toma el contenido de la cadena $a ($b contiene el
valor 4)*/
$b = $a;
/* La cadena $b apunta al mismo contenido que $a (la misma dirección
de memoria para ambas variables)*/
$b = &$a;
// Ambas variables $a y $b contienen el valor 10.
$a=10;
?>
