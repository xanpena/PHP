<?php

    class A {
        function ejemplo(){
            echo "Hola desde A";
        }
    }

    class B extends A{
        function ejemplo(){
            echo "hola desde B";
            parent:ejemplo();
        }
    }

    $b = new B;

    $b->ejemplo();

?>