<?php
    /* Regresa una matriz conteniendo todos los valores de array1 que tienen llaves que no están presentes en cualquier de los otros argumentos. Nota que la asociatividad es preservada. 

    Esta función es como array_diff() excepto en que la comparación es hecha en las llaves en lugar de en los valores */

    function key_compare_func($key1, $key2){
        if($key1 == $key2)
            return 0;
        else if($key1 > $key2)
            return 1;
        else   
            return -1;
    }

    $array1 = array('blue'=>1, 'red'=>2, 'green'=>3, 'purple'=>4);
    $array2 = array('green'=>5, 'blue'=>6, 'yellow'=>7, 'cyan'=>8);

    var_dump(array_diff_ukey($array1, $array2, 'key_compare_func'));
?>