<?php
    /* Comprueba las diferencias entre matrices teniendo en cuenta los índices */
    $array1 = array('a'=>'verde', 'b'=>'negro', 'c'=>'azul', 'rojo');
    $array2 = array('a'=>'verde', 'amarillo', 'rojo');
    $result = array_diff_assoc($array1, $array2);
?>