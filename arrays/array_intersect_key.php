<?php
    /* Regresa una matriz conteniendo todos los valores de array1 los cuales tienen llaves que están presentes en todos los argumentos. */

    $array1 = array('azul', 1, 'rojo'=>2, 'verde'=>3, 'morado'=>4);
    $array2 = array('verde'=>5, 'azul'=>6, 'amarillo'=>7, 'cyan'=>8);

    var_dump(array_intersect_key($array1, $array2));
?>