<?php
    /* Intercambia los valores de una matriz con sus índices */

    // Ejemplo de array_flip()
    $trans = array_flip($trans);
    $original = strtr($str, $trans);

    // Ejemplo de colisión con array_flip()
    $trans = array('a'=>1, 'b'=>1, 'c'=>2);
    $trans = array_flip($trans);
    print_r($trans);

?>