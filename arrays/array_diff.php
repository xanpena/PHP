<?php
    /* Devuelve una matriz que contiene todos los valores de array1 que no aparezcan en ninguna de las otras matrices que se pasan a la función como argumento. Las claves se mantienen. */

    $array1 = array('a'=>'verde', 'rojo', 'azul', 'rojo');
    $array2 = array('b'=>'verde', 'amarillo', 'rojo');
    $resultado = array_diff($array1, $array2);

    echo '<pre>';
        var_dump($resultado);
    echo '</pre>';
?>