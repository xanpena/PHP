<?php
    /* Computa la intersección de arrays con una comprobación adicional de índices */

    $array1 = array('a'=>'green', 'b'=>'yellow', 'c'=>'blue', 'red');
    $array2 = array('a'=>'green', 'yellow', 'red');
    $result_array = array_intersect_assoc($array1, $array2);

    echo '<pre>';
        var_dump($result_array);
    echo '</pre>';
?>