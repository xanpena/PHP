<?php
    /* Aplica la llamada de retorno especificada a los elementos de las matrices dadas */
    function cubo($n){
        return ($n * $n * $n);
    }

    $a = array(1, 2, 3, 4, 5);
    $b = array_map('cubo', $a);
    print_r($b);

    // Ejemplo usando dos matrices
    function mostrar_Castellano($n, $m){
        return("El n&umero; $n es llamado $m en Castellano");
    }

    function map_Castellano($n, $m){
        return(array($n=>$m));
    }

    $a = array(1, 2, 3, 4, 5);
    $b = array('uno', 'dos', 'tres', 'cuatro', 'cinco');

    $c = array_map("mostrar_Castellano", $a, $b);
    print_r($c);

    $d = array_map("map_Castellano", $a, $b);
    print_r($d);

    // Creación de una matriz de matrices
    $a = array(1, 2, 3, 4, 5);
    $b = array('one', 'two', 'three', 'four', 'five');
    $c = array('uno', 'dos', 'tres', 'cuatro', 'cinco');

    $d = array_map(null, $a, $b, $c);
    print_r($d);
?>