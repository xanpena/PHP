<?php
    /* Computa la diferencia entre matrices con un chequeo adicional de índices, el cual es realizado por una llamada de retorno entregada por el usuario */

    function func_comparacion_claves($a, $b){
        if($a === $b){
            return 0;
        }
        return ($a > $b)? 1: -1;
    }

    $matriz1 = array('a'=>'green', 'b'=>'yellow', 'c'=>'blue', 'red');
    $matriz2 = array('a'=>'green', 'yellow', 'red');
    $resultado = array_diff_uassoc($matriz1, $matriz2, "func_comparacion_claves")
?>