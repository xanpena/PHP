<?php
    /* Devuelve un array que contiene todos los valores de array1 que estan presentes en los argumentos. Las claves se preservan */

    $array1 = array('a'=>'green', 'red', 'blue');
    $array2 = array('b'=>'green', 'yellow', 'red');
    $result = array_intersect($array1, $array2);
?>