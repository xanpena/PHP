<?php
  // https://www.youtube.com/watch?v=mUWcVgA__KQ&list=PLpOqH6AE0tNh2Hu2KWoXxTbhHzRgCaHxH&index=6
  // Concepto de herencia
  class Vehiculo{

    public $motor = false;
    public $marca;
    public $color;

    public function estado(){
      if($this->motor)
        echo "El motor está encendido<br>";
      else
        echo "El motor está apagado<br>";
    }

    public function arrancar(){
      if($this->motor){
        echo "Cuidado, el motor ya está encendido<br>";
      }else{
        echo "Ya se ha encendido el motor<br>";
        $this->motor = true;
      }
    }
  }

  class Moto extends Vehiculo{

    public function estadoMoto(){
      $this->estado();
    }
  }

  class Quat extends Moto{

  }

  /*$vehiculo = new Vehiculo;
  $vehiculo->estado();
  $vehiculo->arrancar();
  $vehiculo->estado();*/

  /*$moto = new Moto;
  //$moto->estado();
  $moto->estadoMoto(); */

  $quat = new Quat();
  $quat->estado();

 ?>
