<?php
  // https://www.youtube.com/watch?v=54r7LaWIfWo&index=11&list=PLpOqH6AE0tNh2Hu2KWoXxTbhHzRgCaHxH

  trait PersonaTrait{
    public $nombre;

    public function mostrarNombre(){
      echo $this->nombre;
    }

    public static function metodoStatic(){
    }

    abstract function asignarNombre($nombre);
  }

  trait TrabajadorTrait{
    protected function mensaje(){
      echo " y soy un trabajador";
    }
  }

  class Persona{
    use PersonaTrait, TrabajadorTrait;

    public function asignarNombre($nombre){
      $this->nombre = $nombre . self::mensaje();
    }
  }

  $persona = new Persona;
  $persona->asignarNombre("Xan");
  //echo $persona->nombre;
  $persona->mostrarNombre();
  //$persona->mensaje();
