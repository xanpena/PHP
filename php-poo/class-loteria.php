<?php
  // https://www.youtube.com/watch?v=ZxbRVpGwA84&list=PLpOqH6AE0tNh2Hu2KWoXxTbhHzRgCaHxH&index=4
  class Loteria{

    // Atributos
    public $numero;
    public $intentos;
    public $resultado = false;

    // Métodos
    public function __construct($numero, $intentos){
      $this->numero = $numero;
      $this->intentos = $intentos;
    }

    public function sortear(){
      $minimo = $this->numero / 2;
      $maximo = $this->numero * 20;
      for($i=0; $i<$this->intentos; $i++){
        $ronda = rand($minimo, $maximo);
        self::intentos($ronda);
      }
    }

    public function intentos(){
      if($ronda == $this->numero){
        echo '<strong>'.$ronda.'</strong> = '.$this->numero.'<br>';
        $resultado = true;
      }else{
        echo $ronda." No coincide con".$this->numero.'<br>';
        $resultado = false;
      }
    }

    public function __destruct(){
      if($this->resultado){
        echo "Ganador! :)";
      }else{
        echo "Otra vez será :(";
      }
    }

    $loteria = new Loteria(10,10);
    $loteria->sortear();
  }

?>
