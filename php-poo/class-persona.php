<?php

  class Persona{

      // Atributos
      public $nombre = array();
      public $apellidos = array();

      // Métodos
      public function guardar($nombre, $apellidos){
          $this->nombre[] = $nombre;
          $this->apellidos[] = $apellidos;
      }

      public function mostrar(){
        for($i=0; $i<count($this->nombre); $i++){
          // self::formato($this->nombre[$i], $this->apellido[$i]);
          $this->formato($this->nombre[$i], $this->apellido[$i]);
        }
      }

      public function formato($nombre, $apellidos){
        echo "Nombre: ".$nombre."  | Apellido:".$apellido."<br>";
      }
  }

  $persona = new Persona();
  $persona->guardar("Xan", "Pena");
  $persona->guardar("Cito", "Cuantico");
