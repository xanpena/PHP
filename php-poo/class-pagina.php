<?php
  // https://www.youtube.com/watch?v=tOGABgAhLHo&list=PLpOqH6AE0tNh2Hu2KWoXxTbhHzRgCaHxH&index=7
  // Static Concept
  class Pagina{

    public $nombre = "Juan";
    public static $url = "www.ejemplo.com";

    public function bienvenida(){
      // echo "Bienvenido ".$this->nombre.', la página es'.$this->url.'<br>';
      echo "Bienvenido ".$this->nombre.', la página es'.self::$url.'<br>';
      echo "Bienvenido ".$this->nombre.', la página es'.Pagina::$url.'<br>';
    }

    public static function bienvenida2(){
      //echo "Bienvenido ".$this->nombre;
      echo "Bienvenidos<br />".self::$url;
    }
  }

  class Web extends Pagina{

  }

  $pagina = new Pagina();
  //$pagina->bienvenida();
  //$pagina->bienvenida2();
  //Pagina::bienvenida2();

  Web::bienvenida2();
 ?>
