<?php
 //https://www.youtube.com/watch?v=cmVHqKdFtkQ&index=9&list=PLpOqH6AE0tNh2Hu2KWoXxTbhHzRgCaHxH
  abstract class Molde{

    abstract public function ingresarNombre($nombre);
    abstract public function obtenerNombre();

    public static function mensaje($mensaje){
      print $mensaje;
    }
  }

  class Persona extends Molde{

    private $mensaje  "Hola mundo";
    private $nombre;

    public function mostrar(){
      print $this->mensaje;
    }

    public function ingresarNombre($nombre, $apellido=NULL){
      $this->nombre = $nombre.' '.$apellido;
    }

    public function obtenerNombre(){
      print $this->nombre;
    }
  }

  // //Funciona porque no estamos instanciando nada
  // Molde::mensaje("Hola mundo");

  // No funcionaría hasta que no implementemso los métodos y atributos que obliga Molde
  $obj = new Persona();
  //$obj->mostrar();
  $obj->ingresarNombre("Xan", "Pena");
  $obj->obtenerNombre();

 ?>
