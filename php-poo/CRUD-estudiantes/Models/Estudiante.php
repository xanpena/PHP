<?php namespace Models;

  class Estudiante{

    private $id;
    private $nombre;
    private $edad;
    private $promedio;
    private $imagen;
    private $seccion_id;
    private $fecha;
    private $con;

    private function __construct(){
      $this->con = new Conexion();
    }

    public function set($atributo, $contenido){
      $this->$atributo = $contenido;
    }

    public function get($atributo){
      return $this->$atributo;
    }


    public function listar(){
      $sql = "SELECT t1.*, t2.nombre as seccion FROM estudiantes t1 INNER JOIN secciones t2 ON t1.seccion_id=t2.id";
      $datos= $this->con->consultarRetorno($sql);
      return $datos;
    }

    public function add(){
      $sql = "INSERT INTO estudiantes(id, nombre, edad, promedio, imagen, seccion_id, fecha) VALUES (null, '{$this->nombre}', '{$this->edad}', '{$this->promedio}', '{$this->imagen}', '{$this->seccion_id}', NOW())";
      $this->con->consultaSimple($sql);
    }

    public function delete(){
      $sql = "DELETE FROM estudiantes WHERE id= '{$this->id}'";
      $this->con->consultaSimple($sql);
    }

    public function edit(){
      $sql = "UPDATE FROM estudiantes SET nombre = '{$this->nombre}', '{$this->edad}', '{$this->promedio}', '{$this->seccion_id}' WHERE id = '{$this->id}'";
      $this->con->consultaSimple($sql);
    }

    public function view(){
      $sql = "SELECT t1.*, t2.nombre as seccion FROM estudiantes t1 INNER JOIN secciones t2 ON t1.seccion_id=t2.id WHERE t1.id = '{$this->id}'";
      $datos = $this->con->consultarRetorno($sql);
      $row = mysqli_fetch_assoc($datos)
      return $row;
    }
  }
