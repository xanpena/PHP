<?php
  // https://www.youtube.com/watch?v=vzlR-Lwa5K0&index=8&list=PLpOqH6AE0tNh2Hu2KWoXxTbhHzRgCaHxH
  interface Automovil{

    public function encender();
    public function apagar();
  }

  interface gasolina extends Automovil{
    public function vaciarTanque();
    public function llenarTanque($litros);
  }

  class Deportivo implements gasolina{
    /*public function ver(){
      echo "Hola";
    }*/

    private $estado = false;
    private $tanque = 0;

    public function estado(){
      if($this->estado)
        print "Encendido!<br>";
      else
        print "Apagado!<br>";
    }

    public function encender(){
      if($this->estado){
        print "El automovil ya se encontraba encendido<br>";
      }else{
        if($this->tanque <= 0){
          print "No tienes gasolina para encender el automovil<br>";
        }else{
          print "Automovil encendido!<br>";
          $this->estado = true;
        }
      }
    }

    public function apagar(){
      if($this->estado){
        print "Automovil apagado!<br>";
        $this->estado = false;
      }else{
        print "El automovil ya se encontraba apagadp<br>";
      }
    }

    public function vaciarTanque(){
      $this->tanque = 0;
    }

    public function llenarTanque($litros){
      $this->tanque = $litros;
    }

    public function usar($km){
      if($this->estado){
        $consumo = $km / 3;
        $this->tanque = $this->tanque - $consumo;
        if($this->tanque<=0){
          $this->estado = false;
          print "Te quedaste sin gasolina!<br>";
        }
      }else{
        print "Debes encender el automovil para poder usarlo";
      }
    }
  }

  $obj = new Deportivo;
  //$obj->ver();
  $obj->llenarTanque(100);
  $obj->encender();
  $obj->usar(20);
  $obj->estado();
  $obj->usar(120);
  $obj->estado();
  $obj->usar(50);
  $obj->estado();
  $obj->usar(350);
  $obj->estado();


 ?>
