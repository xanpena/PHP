<?php
  // https://www.youtube.com/watch?v=7ETYvGi4m18&list=PLpOqH6AE0tNh2Hu2KWoXxTbhHzRgCaHxH&index=12

  //  require_once "api/Models/Persona.php"

  spl_autoload_register(function($clase){
      // print $clase;
      $ruta = "api/" . str_replace("\\", "/", $clase);
      //print $ruta;
      if(is_readable($ruta)){
        require_once $ruta;
      }else{
        echo "El archivo no existe o está corrupto";
      }
  });

  // Persona::hola();
  Models\Persona::hola();
  Controllers\PersonasController::hola();
