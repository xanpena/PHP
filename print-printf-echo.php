<!DOCTYPE html>
<html>
<head>
	<title>Print, printf y echo</title>
</head>
<body>
	<?php 
	// sprintf(), print_r()
		printf("<h1>Uso de la orden print</h1>");
		$i = 10;
		// printf ("cadena de formato", argumento1, argumento2,...)
		printf("Valor de la variable \$i= %d \n", $i);
	echo '<br />';
		printf("Valor de la variable \$i= %%d \n", $i);
	echo '<br />';
		printf("La variable \$i con la directiva %%05d aparece como %05d \n", $i);
	echo '<br />';
		// La salida numérica aparece en formato binario (base 2).
		printf("\$i con %%b aparece como %b", $i);
		echo '<br />';
		// La salida completa aparece como un carácter que se indica detrás de la C.
		printf("\$i con %%CZ aparece como %CZ", $i);
		echo '<br />';
		// La salida numérica aparece en formato decimal (base 10).
		printf("\$i con %%d aparece como %d", $i);
		echo '<br />';
		// La salida numérica aparece en formato de coma flotante.
		printf("\$i con %%f aparece como %f", $i);
		echo '<br />';
		// La salida numérica aparece en formato octal (base 8).
		printf("\$i con %%o aparece como %o", $i);
		echo '<br />';
		// La salida, aunque sea numérica, aparece como una cadena.
		printf("\$i con %%s aparece como %s cadena con números", $i);
		echo '<br />';
		// La salida numérica aparece en formato hexadecimal (base 16) con las letras en minúscula.
		printf("\$i con %%04x aparece como %04x", $i);
		echo '<br />';
		//La salida numérica aparece en formato hexadecimal (base 16) con las letras en mayúscula.
		printf("\$i con %%04X aparece como %04X", $i);


	?>
</body>
</html>