<?php
define('PROJECT_PATH', dirname(path:__DIR__));
define('APPPATH', PROJECT_PATH, '/App');

function autoload_classes($class_name){
  $filename = PROJECT_PATH.'/'.str_replace(search:'\\', replace:'/', $class_name).'.php';
  if(is_file($filename)){
    include_once $filename;
  }
}

spl_autoload_register(autoload_function:'autoload_classes');
