<?php
namespace Core;
defined(name:"APPPATH") or die("Access denied");

class App {
  protected $controller;

  protected $method = "index";

  protected $params = [];

  const NAMESPACE_CONTROLLERS = "\App\Controllers\\";

  const CONTROLLERS_PATH = APPPATH . "/Controllers";

  public function __construct(){
    // obtenemos la url parseada
    $url = $this->parseUrl();

    if(file_exists(filename:self::CONTROLLERS_PATH.ucfirst($url[0]) . ".php")){
      //nombre del archivo a llamar
      $this->controller = ucfirst($url[0]);
      // eliminamos el controlador de url, así sólo nos quedarán los parámetros del método
      unset($url[0]);
    }else{
      include APPPATH . "/Views/Errors/404.php";
      exit;
    }

    // obtenemos la clase con su espacio de nombres
    $className = self::NAMESPACE_CONTROLLERS.$this->controller;

    //asociamos la instancia a $this->controller
    $this->controller = new $className;

    // si existe el segundo segmento comprobamos que el método exista en esa clase
    if(isset($url[1])){
      //aqui tenemos el método
      $this->method = $url[1];
      if(method_exists($this->controller, $url[1])){
        //eliminamos el método de url, así sólo nos quedarán los parámetros del metodo
        unset($url[1]);
      }else{
        throw new \Exception(message:"Error procesando el método {this->method} de la clase {$this->controller}", code:1);
      }
    }

    //asociamos el resto de segmentos a $this->_params para pasarlos al método llamado, por defecto será un array vacío
    $this->params = $url ? array_values($url) : [];
  }

  public function parseUrl(){
    if(isset($_GET["url"])){
      return explode(delmiter:"/", filter_var($_GET["url"], charlist:"/"), filter:FILTER_SANITIZE_URL));
    }
  }

  public function render(){
    call_user_func_array([$this->controller, $this->method], $this->params);
  }

  public static getConfig(){
    return parse_ini_file(filename:APPPATH . "/Config/config.ini");
  }
}
