<?php
	// Obtener el primer caracter de una cadena
	$cadena = 'Esta es una prueba.';
	$primer = $cadena{0};

	// Obtener el tercer caracter de una cadena
	$tercer = $cadena{2};

	// Obtener el último caracter de una cadena
	$cadena = 'Esto también es una prueba.';
	$ultimo = $cadena{strlen($cadena)-1};

	// Modificar el último carácter de una cadena
	$cadena = 'Observer el mar'; 
	$cadena{strlen($cadena) -1} = '1'
?>