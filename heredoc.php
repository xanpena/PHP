<?php
	// El soporte heredoc fue agregado en PHP 4
	$cadena = <<<FIN
	Ejemplo de una cadena
	que se extiende en varias l&iacute;neas
	usando la sintaxis heredoc
	FIN;

	/* un ejemplo mas complejo con variables */

	class foo{
		var $foo;
		var $bar;

		function foo(){
			$this->foo = 'Foo';
			$this->bar = array('Bar1', 'Bar2', 'Bar3');
		}
	}

	$foo = new foo();
	$nombre = 'MiNombre';

	echo <<<FIN
	Mi nombre es "$nombre". Estoy imprimiendo algo de $foo->foo.
	Ahora, estoy imprimiendo algo de {$foo->bar[1]}.
	Esto deber&iacute;a imprimir una letra 'A' may&uacute;scula: \x41
	FIN;
?>