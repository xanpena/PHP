<?php
include "espacio.php";
include "subespacio.php";

echo '<h1>Espacio de nombres</h1>';



function funcionEjemplo(){
	echo "Ejecutando funcionEjemplo() desde index.php<br>";
}

// La barra refiere al espacio de nombres raíz.
funcionEjemplo();
\funcionEjemplo();

PrimerEspacio\funcionEjemplo();
\PrimerEspacio\funcionEjemplo();

\PrimerEspacio\Subespacio\funcionEjemplo();