<?php

    $makefoo = true;
    /* No podemos llamar a foo() desde aquí xq todavía no existe, pero podemos llamar a la función bar() */
    bar();

    if($makefoo){
        function foo(){
            echo "Hola desde foo()";
        }
    }

    /* Ahora si que podemos llamar a foo(), tras evaluarse $makefoo a true */

    if($makefoo) foo();

    function bar(){
        echo "Hola desde bar()";
    }

?>