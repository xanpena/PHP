<?php

    class Example{
        private static $instance;

        private function __construct(){
            echo 'Hola desde el constructor';
        }

        public static function singleton(){
            if(!isset(self::$instance)){
                $c = __CLASS__;
                self::$instance = new $c;
            }

            return self::$instance;
        }

        public function bark(){
            echo 'Hola desde bark';
        }

        public function __clone(){
            trigger_error('Clonado no permitido', E_USER_ERROR);
        }
    }


    $test = new Example;

    $test = Example::singleton();
    $test->back();

    $test_clone = clone($test);

?>