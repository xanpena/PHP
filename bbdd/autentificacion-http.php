<?php

    if(!isset($_SERVER['PHP_AUTH_USER'])){
        header('WWW-Authenticate: Basic realm="My Realm"');
        header('HTTP/1.0 401 Unauthorized');
        echo 'Texto a mostrar si usuario pulsa botón Cancelar';
        exit;
    }else{
        echo "<p>Hola {$_SERVER['PHP_AUTH_USER']}.</p>";
        echo "Tu contraseña es: {$_SERVER['PHP_AUTH_PW']}";
    }
?>