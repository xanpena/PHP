<?php
	// ejemplo simple de una llamada de retorno
	function mi_llamada_de_retorno(){
		echo "Hola mundo!";
	}

	call_user_func('mi_llamada_de_retorno');

	// ejemplos de método como llamada de retorno

	class Miclase{
		function miMetodoDeRetorno(){
			echo "Hola mundito!";
		}
	}

	// llamada de método estático de clase sin instanciar un objeto
	call_user_func(array('MiClase', 'miMetodoDeRetorno'));

	// llamada a un método de objeto
	$obj = new MiClase();
	call_user_func(array(&$obj, 'miMetodoDeRetorno'));

?>